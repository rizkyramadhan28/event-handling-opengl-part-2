from OpenGLContext import testingcontext
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math

# Koordinat untuk posisi persegi

koordinat_x = 0
koordinat_y = 0

# Warna default untuk persegi

red_color = 1
green_color = 1
blue_color = 1

# Palette warna untuk background dan persegi:

# Merah: 214.0 / 255.0, 48.0 / 255.0, 49.0 / 255.0, 1.0
# Hijau: 0.0, 184.0 / 255.0, 148.0 / 255.0, 1.0
# Dark: 44.0 / 255.0, 62.0 / 255.0, 80.0 / 255.0 , 1.0
# Biru: 41.0 / 255.0, 128 / 255.0, 185 / 255.0 , 1.0

# Warna untuk persegi dan background

warna_persegi = "Putih"
warna_background = "Hitam"

# Untuk pergantian warna

total_clicked = 0


def init():
    glClearColor(0.0, 0.0, 0.0, 1.0)
    gluOrtho2D(-500.0, 500.0, -500.0, 500.0)


# Membuat teks dengan menggunakan bitmap

def drawBitmapText(string, x, y, z):
    glRasterPos3f(x, y, z)
    for c in string:
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ord(c))

# Membuat persegi


def draw_quads_object():

    global red_color
    global green_color
    global blue_color

    glColor3f(red_color, green_color, blue_color)

    glBegin(GL_QUADS)
    # Top left
    glVertex2f(-50 + koordinat_x, 50 + koordinat_y)

    # Top right
    glVertex2f(50 + koordinat_x, 50 + koordinat_y)

    # Bottom right
    glVertex2f(50 + koordinat_x, -50 + koordinat_y)

    # Bottom left
    glVertex2f(-50 + koordinat_x, -50 + koordinat_y)
    glEnd()

# Untuk menggerakkan persegi dan mengubah warna background


def get_user_keyboard_input(key, x, y):

    global koordinat_x
    global koordinat_y
    global warna_background

    # Untuk mengubah posisi persegi

    if key == GLUT_KEY_UP:
        koordinat_y += 10
        print(f"UP, x: {koordinat_x}, y: {koordinat_y}")

    elif key == GLUT_KEY_DOWN:
        koordinat_y -= 10
        print(f"DOWN, x: {koordinat_x}, y: {koordinat_y}")

    elif key == GLUT_KEY_LEFT:
        koordinat_x -= 10
        print(f"LEFT, x: {koordinat_x}, y: {koordinat_y}")

    elif key == GLUT_KEY_RIGHT:
        koordinat_x += 10
        print(f"RIGHT, x: {koordinat_x}, y: {koordinat_y}")

    # Untuk mengubah warna background

    # Atas kanan => warna hijau

    if koordinat_x > 0 and koordinat_y > 0:
        glClearColor(0.0, 184.0 / 255.0, 148.0 / 255.0, 1.0)
        warna_background = "Hijau"

    # Atas kiri => warna merah

    if koordinat_x < 0 and koordinat_y > 0:
        glClearColor(214.0 / 255.0, 48.0 / 255.0, 49.0 / 255.0, 1.0)
        warna_background = "Merah"

    # Bawah kiri => warna gelap

    if koordinat_x < 0 and koordinat_y < 0:
        glClearColor(44.0 / 255.0, 62.0 / 255.0, 80.0 / 255.0, 1.0)
        warna_background = "Gelap"

    # Bawah kanan => warna biru

    if koordinat_x > 0 and koordinat_y < 0:
        glClearColor(41.0 / 255.0, 128 / 255.0, 185 / 255.0, 1.0)
        warna_background = "Biru"

    glutPostRedisplay()

# Untuk mengubah warna persegi dengan menggunakan mouse


def get_mouse_user_input(button, state, x, y):

    global red_color
    global green_color
    global blue_color
    global total_clicked
    global warna_persegi

    # Jika user melakukan klik kanan atau klik kiri

    if (button == GLUT_RIGHT_BUTTON and state == GLUT_DOWN) or (button == GLUT_LEFT_BUTTON and state == GLUT_DOWN):

        # Persegi berwarna merah

        if total_clicked == 0:
            red_color = 214.0 / 255.0
            green_color = 48.0 / 255.0
            blue_color = 49.0 / 255.0

            warna_persegi = "Merah"

        # Persegi berwarna gelap

        elif total_clicked == 1:
            red_color = 44.0 / 255.0
            green_color = 62.0 / 255.0
            blue_color = 80.0 / 255.0

            warna_persegi = "Gelap"

        # Persegi berwarna hijau

        elif total_clicked == 2:
            red_color = 0.0
            green_color = 184.0 / 255.0
            blue_color = 148.0 / 255.0

            warna_persegi = "Hijau"

        # Persegi berwarna biru

        else:
            red_color = 41.0 / 255.0
            green_color = 128 / 255.0
            blue_color = 185 / 255.0

            warna_persegi = "Biru"

        total_clicked += 1

    if total_clicked > 3:
        total_clicked = 0

    draw_quads_object()
    glutPostRedisplay()


def display():
    glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1, 1, 1)

    drawBitmapText("Warna:", -450, -350, 0)
    drawBitmapText(f"Objek: {warna_persegi}", -450, -400, 0)
    drawBitmapText(f"Background: {warna_background}", -450, -450, 0)

    glBegin(GL_LINES)
    glVertex2f(-500.0, 0.0)
    glVertex2f(500.0, 0.0)

    glVertex2f(0.0, 500.0)
    glVertex2f(0.0, -500.0)
    glEnd()

    draw_quads_object()

    glFlush()
    glutPostRedisplay()
    glutSwapBuffers()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Keyboard and Mouse Event")
    glutDisplayFunc(display)

    init()
    glutSpecialFunc(get_user_keyboard_input)
    glutMouseFunc(get_mouse_user_input)
    glutMainLoop()


main()
